<?php
  // In a real module variables should be manipulated in a preprocess function.
  $content = $element->content;
?>

<div class="<?php print $classes; ?>">
  <?php print render($content['title']); ?>
  <div class="availability">
	<?php print render($content['field_availability']); ?>
  </div>

  <div class="position_primary">
    <?php print render($content['field_position_1']); ?>
  </div>

  <div class="position_secondary">
    <?php print render($content['field_position_2']); ?>
  </div>

  <div class="position_3">
    <?php print render($content['field_position_3']); ?>
  </div>

  <div class="comments">
    <?php print render($content['field_comments']); ?>
  </div>

</div>