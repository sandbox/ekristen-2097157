<?php

/**
 * Draft Draft Page
 */
function draft_draft_page($node) {
  global $user;

  if ($node->draft_status != DRAFT_STATUS_FINISHED && !in_array('administrator', array_values($user->roles)) && !tournament_admins_access($node) && !_draft_admin_access($node) && !_draft_is_captain($node)) {
    drupal_goto('http://draft.tribaloutpost.com');
  }

  $draft_order = db_select('draft_order', 'do')->fields('do')->condition('nid', $node->nid)->orderBy('ordr', 'ASC')->execute()->fetchAllAssoc("ordr");

  $participants = tournament_participants_data($node);

  $headers[] = array('data' => 'Draft Order');

  foreach ($draft_order as $order) {
    $headers[] = array('data' => $order->ordr);
  }

  $rounds = draft_number_of_rounds($node);

  for ($x = 0; $x < $rounds; $x++) {
    $picks = db_select('draft_picks', 'dp')->fields('dp')->condition('nid', $node->nid)->condition('round', $x)->execute()->fetchAllAssoc('tid');

    $row = array();
    if ($x == 0) {
      $row[] = array('data' => t('Team (Captain)'), 'class' => 'header');
    }
    else {
      if ($x / 2 == is_int($x / 2)) {
        $dir = '<<<';
      }
      else {
        $dir = '>>>';
      }
      $row[] = array('data' => "Round {$x} {$dir}");
    }

    foreach ($draft_order as $order) {
      $row_class = "rnd{$x}ord{$order->ordr}";
      if (isset($picks[$order->tid]) && $picks[$order->tid]->round == $x) {
        $u = user_load($picks[$order->tid]->uid);
        if ($x == 0) {
          $team   = array_shift(entity_load('team', array($order->tid)));
          $teamlk = l($team->name, "team/{$team->tid}");
          $userlk = l($u->name, "node/{$node->nid}/draft/{$u->uid}");
          $row[]  = array('data' => "{$teamlk} ({$userlk})", 'class' => $row_class);
        }
        else {
          $row[] = array('data' => l($u->name, "node/{$node->nid}/draft/{$u->uid}"), 'class' => $row_class);
        }
      }
      else {
        $row[] = array('data' => '', 'class' => $row_class);
      }
    }

    $rows[] = $row;
  }

  $draft_participants = db_select('draft_participants', 'dp2')->fields('dp2', array('uid'))->condition('nid', $node->nid)->execute()->fetchAll();
  $picks = db_select('draft_picks', 'dp')->fields('dp', array('uid'))->condition('nid', $node->nid)->execute()->fetchAll();

  $complete = number_format(count($picks) / count($draft_participants), 2) * 100;

  $output = '';
  $message = 'Draft Complete';
  if ($complete != 100) {
    drupal_add_js(drupal_get_path('module', 'draft') . '/js/draft.js', 'file');

    drupal_add_js(array(
        'draft' => array(
          'settings' => 'test',
          'args' => arg(),
          'query' => block_refresh_get_query_as_string(),
        ),
      ), 'setting');

    $current_team = array_shift(entity_load('team', array($node->draft_turn)));

    $output = '<span class="alert alert-info">This page refreshes itself.</span>';
    $message = "Team {$current_team->name} is picking.";
  }

  $output .= theme('progress_bar', array('percent' => $complete, 'message' => $message));
  $output .= theme('table', array('header' => $headers, 'rows' => $rows));

  return $output;
}

/**
 * Ajax Callback for Page
 */
function draft_draft_page_ajax($node) {
  print render(draft_draft_page($node));
  drupal_exit();
}

function draft_draft_pick_form($form, &$form_state, $node, $user) {
  if (!_draft_my_turn($node)) {
    $form['description'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('It is not your turn to pick!'),
      '#weight' => '-50',
    );
    return $form;
  }

  $form_state['node'] = $node;
  $form_state['user'] = $user;
  $form_state['team'] = array_shift(entity_load('team', array($node->draft_turn)));

  // Just a captain not an admin
  if (!tournament_admins_access($node)) {
    $results = db_select('teams_members', 'tm')->fields('tm', array('tid'))->condition('role', TEAM_MEMBER_ROLE_ADMIN)->execute()->fetchAll();

    $form['description'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('By clicking Draft below, you will be drafting <strong>!name</strong> for your team.', array('!name' => $user->name)),
      '#weight' => '-50',
    );
  }
  else {
    $form['description'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('By clicking Draft below, you will be drafting participant <strong>!name</strong> for team <strong>!team</strong> using your admin powers.', array('!name' => $user->name, '!team' => $form_state['team']->name)),
      '#weight' => '-50',
    );
    $form['understand'] = array(
      '#type' => 'checkbox',
      '#title' => 'I understand that I am using my admin powers to draft a player to a team.',
    );
  }

  // Define Form Actions
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Draft Participant'),
  );
  return $form;
}

/**
 * Validate Callback
 */
function draft_draft_pick_form_validate($form, &$form_state) {
  if (isset($form_state['values']['understand']) && $form_state['values']['understand'] != 1) {
    form_set_error('understand', t('You must acknowledge you understand.'));
  }
}

/**
 * Submit Callback
 */
function draft_draft_pick_form_submit($form, &$form_state) {
  // Add Pick
  db_insert('draft_picks')->fields(array('nid' => $form_state['node']->nid, 'tid' => $form_state['team']->tid, 'uid' => $form_state['user']->uid, 'round' => $form_state['node']->draft_round, 'picked' => REQUEST_TIME))->execute();

  $draft_participants = db_select('draft_participants', 'dp2')->fields('dp2', array('uid'))->condition('nid', $form_state['node']->nid)->execute()->fetchAll();
  $picks = db_select('draft_picks', 'dp')->fields('dp', array('uid'))->condition('nid', $form_state['node']->nid)->execute()->fetchAll();
  $percentage_complete = number_format(count($picks) / count($draft_participants), 2) * 100;
  module_invoke_all('draft_percentage', $percentage_complete);

  // Add user to team
  team_member_insert($form_state['team'], $form_state['user'], TEAM_MEMBER_ROLE_MEMBER);

  // Update Round
  _draft_update_round($form_state['node']);

  // Update Turn
  _draft_update_turn($form_state['node']);

  drupal_set_message(t('You have successfully drafted !user!', array('!user' => $form_state['user']->name)));

  $hook_data = array(
    'node' => $form_state['node'],
    'team' => $form_state['team'],
    'user' => $form_state['user'],
    'round' => $form_state['node']->draft_round,
  );

  module_invoke_all('draft_pick', $hook_data);


  $form_state['redirect'] = "node/{$form_state['node']->nid}/draft";
}


function draft_manage_participants_page($node) {
	drupal_set_title(t('Manage Draft Participants'));

  $participants = db_select('draft_participants', 'dp')
    ->fields('dp')
    ->condition('nid', $node->nid)
    ->execute()
    ->fetchAll();

  foreach ($participants as $participant) {
    $ids[] = $participant->uid;
  }

  if (!isset($ids)) {
    return t('No participants signed up for this tournament.');
  }

  // Load participant entities at once
  $entities = entity_load('user', $ids);

  

  $header = array(
    array('data' => t('Name'), 'width' => '40%'),
    array('data' => t('Registered')),
    array('data' => t('Action')),
  );

  // Determine action access just once to save a little memory
  $access = $node->tournament_status != TOURNAMENT_STATUS_FINISHED;

  $rows = array();
  foreach ($participants as $participant) {
    $id = $participant->uid;

    if (isset($entities[$id])) {
      $entity = $entities[$id];
      $actions = array();

      // Set up a table row
      $row = array(
        'label' => l($entity->name, 'user/' . $id),
        'registered' => format_date($entity->created),
      );

      $actions[] = l(t('IPs'), 'user/' . $id . '/ip', array('attributes' => array('class' => 'btn btn-mini btn-info')));
      $actions[] = l(t('Aliases'), 'user/' . $id . '/aliases', array('attributes' => array('class' => 'btn btn-mini btn-info')));
      $actions[] = l(t('Remove'), 'node/' . $node->nid . '/edit/draft_participants/' . $id . '/remove', array('attributes' => array('class' => 'btn btn-mini btn-danger')));

      // Convert actions into links
      $row['actions'] = !empty($actions) ? implode(' ', $actions) : '';

      // Add the participant to the correct table
      $rows[] = $row;
    }
  }

  $build['participants'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No participants signed up for this tournament.'),
  );

  return $build;
}


function draft_manage_participants_page_remove($form, &$form_state, $node, $user) {
	$form['description'] = array(
		'#markup' => 'Are you really sure you want to remove user ' . $user->name . ' from the draft? <br><br>',
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Remove'
	);

	$form_state['node'] = $node;
	$form_state['user'] = $user;

	return $form;
}


function draft_manage_participants_page_remove_submit($form, &$form_state) {

	db_delete('draft_participants')
		->condition('nid', $form_state['node']->nid)
		->condition('uid', $form_state['user']->uid)
		->execute();

	drupal_set_message(t('Draft participant has been removed form the draft'));

	// Redirect to the manage participants page
  	$form_state['redirect'] = 'node/' . $form_state['node']->nid . '/edit/draft_participants';
}