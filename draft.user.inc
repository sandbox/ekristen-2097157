<?php

function _draft_user_page_title_callback($user, $node) {
	return t('Draft Information for') . ' "' . $user->name . '"';
}

/**
 * Status
 */
function draft_user_page($user, $node, $view_mode = 'full') {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', "draft_{$node->type}_participant", '=');
  $query->propertyCondition('uid', $user->uid, '=');
  $query->propertyCondition('nid', $node->nid, '=');
  $results = $query->execute();

  $pids = array();
  foreach ($results["draft_{$node->type}_participant"] as $result) {
    $pids[] = $result->pid;
  }

  $entity = array_shift(entity_load("draft_{$node->type}_participant", $pids));
  $new_entity = $entity;
  $entity->content = array();

  //field_attach_prepare_view("draft_{$node->type}_participant", array($entity), $view_mode);
  //entity_prepare_view("draft_{$node->type}_participant", array($entity));

  $entity->content += field_attach_view("draft_{$node->type}_participant", $entity, $view_mode);

  $entity->content += array(
    '#theme' => 'draft_participant',
    '#element' => $new_entity,
    '#view_mode' => 'full',
    '#language' => NULL,
  );

  return $entity->content;
}

